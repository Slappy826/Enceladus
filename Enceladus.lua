--[[
	File        : Enceladus.lua
	Author      : Mathematical Dessert
	File Type   : ModuleScript
	
	Description : The interface between the public parts of Enceladus and the private, interior.
	
	Change Log :
	09/02/2016 - Pkamara - Started writing
]]--

--[[ Module Table ]]--

local Enceladus = {}

--[[ Local Definitions ]]--

local Workspace = game:GetService("Workspace")
local Players = game:GetService("Players")
local ReplicatedStorage  = game:GetService("ReplicatedStorage")
local MarketplaceService = game:GetService("MarketplaceService")
local ServerScriptService = game:GetService("ServerScriptService")
local RunService = game:GetService("RunService")

--[[ Settings ]]--

local Settings = {
    ["AutoUpdate"] = false,
}

--[[ Run Defintions ]]--

local IsServer  = RunService:IsServer()
local IsClient  = RunService:IsClient()
local IsStudio  = RunService:IsStudio()
local IsRunMode = RunService:IsRunMode()