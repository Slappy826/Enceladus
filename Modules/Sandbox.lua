--[[
    File Name: Sandbox.lua
    Author: Pkamara
    Description: Easy to use sandbox module for stopping arb code

    ChangeLog -
        26/02/2016 - Started writing the sandbox

    Future Ideas -
        - An address checker
        - Stop leaking environments
]]--

--[[ Internal Definitions ]]--

local InternalSandbox = {
    FakeObjects = {},
    RealObjects = {},
    Environments = {},
}

local Sandbox = {}

local DangerousObjects = { -- Objects that are secured
    ["Players"] = true,
    ["Player"] = true,
    ["PlayerGui"] = true,
    ["Backpack"] = true,
    ["StarterPack"] = true,
    ["StarterGui"] = true
}

local FakeObjects, RealObjects = InternalSandbox.FakeObjects, InternalSandbox.RealObjects

--[[ Settings ]]--

local DEBUG_MODE = false -- Outputs debug messages
local KEY_LOCKED = true  -- Locks the script to a certain key
local PLACE_LOCK = true  -- Locks the script to only load in certain places
local LOCAL_MODE = false -- Is it a local script

local PRE_LD_KEY = "Some Key" -- For the Load if KEY_LOCKED is enabled

local PLACES_IDS = {
    [314145399] = true,
    [191240586] = true
}

--[[ Upvalues ]]--

local Environment = getfenv(0)
local tostring = tostring
local error = error
local wait = wait
local getfenv = getfenv
local setfenv = setfenv
local select = select
local unpack = unpack
local xpcall = xpcall
local type = type
local ypcall = ypcall
local setmetatable = setmetatable
local getmetatable = getmetatable
local newproxy = newproxy
local require = require
local next = next
local Instance = Instance
local game = game
local Game = game
local workspace = workspace
local Workspace = workspace
local pcall = pcall
local loadstring = loadstring
local rawset = rawset
local rawget = rawget
local tostring = tostring
local shared = shared
local LoadLibrary = LoadLibrary

local RealObject, FakeObject, FakeInstance, Network

--[[ Instance Indexes ]]--

local function IsRealObject(Instance)
    if Instance == nil then
        return false
    end

    if not pcall(function() return game.IsA(Instance, "Instance") end) then
        return false
    end

    if not game.IsA(Instance, "Instance") then
        return false
    else
        return true
    end
end

local function IsObjectDangerous(Object)
    if DangerousObjects[Object.className] then
        return true
    else
        return false
    end
end

local function GetResult(Function, ...)
    local Check, Result = pcall(Function, ...)

    return Check, Result
end

local function GetRandomPlayer()
    return game:GetService("Players"):GetPlayers()[1]
end

local ReplacementFunctions = {
    Destroy = function(self)
        self = RealObject(self)
        if IsRealObject(self) and IsObjectDangerous(self) then
            return error("You cannot use the method 'Destroy' on this Instance")
        else
            local Check, Response = GetResult(game.Destroy, self)
            if not Check then
                return error(Response)
            else
                return Response or nil
            end
        end
    end,

    Remove = function(self)
        self = RealObject(self)
        if IsRealObject(self) and IsObjectDangerous(self) then
            return error("You cannot use the method 'Remove' on this Instance")
        else
            local Check, Response = GetResult(game.Remove, self)

            if not Check then
                return error(Response)
            else
                return Response or nil
            end
        end
    end,

    Kick = function(self)
        self = RealObject(self)
        if IsRealObject(self) and IsObjectDangerous(self) then
            return error("You cannot use the method 'Kick' on this Instance")
        else
            local Player = GetRandomPlayer()

            if not Player then
                return nil
            end

            local Check, Response = GetResult(Player.Kick, self)

            if not Check then
                return error(Response)
            else
                return Response or nil
            end
        end
    end
}

local SandboxIndex = {
    Get = {
        Destroy = ReplacementFunctions.Destroy,
        Remove = ReplacementFunctions.Remove,
        Kick = ReplacementFunctions.Kick
    },

    Set = {

    },
}

local function GetReplacedFunction(Type, Name)
    return SandboxIndex[Type][Name] or nil
end

--[[ Your Custom Functions ]]--

local function Gen(User, Type, ...)
    local args = {...}
    for i = 1, select("#",...) do
        args[i] = tostring(args[i])
    end

    Network:FireOutput(User, Type, ((table.concat(args,"\t") or nil)))
end

--[[ Sandbox Functions ]]--

function RealObject(...)
    local Objects = {...}

    for i,v in next, Objects do
        local newRealObject = RealObjects[v]

        Objects[i] = newRealObject or v
    end

    return unpack(Objects)
end

local function GetMember(Object, Index)
    return Object[Index] or nil
end

local function SetMemeber(Object, Index, Value)
    Object[Index] = Value
end

function FakeObject(...)
    local Objects = {...}

    for ObjIndex, RealObj in next, Objects do
        local RealType = type(RealObj)
        local NewFakeObject

        if FakeObjects[RealObj] then
            NewFakeObject = FakeObjects[RealObj]
		elseif RealObjects[RealObj] then
			NewFakeObject = RealObj
        else
            if RealType == "table" then
                NewFakeObject = RealObj

                for i,v in next, NewFakeObject do
                    NewFakeObject[i] = FakeObject(v)
                end
			elseif RealType == "function" then
				NewFakeObject = function(...)
					return FakeObject(RealObj(RealObject(...)))
				end
            elseif RealType == "userdata" then
                if pcall(game.IsA, game, RealObj, "Instance") then
                    NewFakeObject = FakeInstance(RealObj)
                elseif tostring(RealObj):find("Signal") == 1 then
                    local Check, RealEvent = pcall(game.Changed.connect, RealObj, function() end)
                    if not Check then
                        return nil
                    else
                        RealEvent:disconnect()
                    end

                    local NewEvent = newproxy(true)
                    local EventMetatable = getmetatable(NewEvent)
                    local InternalTable = setmetatable({},{__metatable = getmetatable(RealEvent)})
                    local ToString = tostring(RealObj)

                    function EventMetatable:__tostring()
                       return ToString
                    end                    
                    
                    function EventMetatable:wait()
                        return FakeObject(RealObj.wait(RealObject(self)))
                    end
                    
                    function EventMetatable:connect(Function)
                        local Return
						local Connection
                        
                        local Connection = RealObj.connect(RealObject(self), function(...)
                            if type(Function) ~= "function" then
                                pcall(function() Connection:disconnect() end)
                                return error("Attempt to connect failed: Passed value is not a function")
                            end
                            
                            local Success, Result = pcall(FakeObject(Function), FakeObject(...))
                            
                            if not Success then
                                pcall(function() Connection:disconnect() end)
                                spawn(function() error(Result, 0) end)
                                
                                return warn("Disconnected event because of exception")
                            end
                        end)
                        
                        local FakeConnection = newproxy(true)
                        local FakeConnectionMeta = getmetatable(FakeConnection)
                        local FakeConData = setmetatable({},{__metatable = "This metatable is locked"})
                        
                        function FakeConData:disconnect()
                            pcall(function() Connection:disconnect() end)
                            FakeConData.connected = false
                        end
                        
                        function FakeConnectionMeta:__index(Index)
                            if FakeConData[Index] then
                                return FakeConData[Index]
                            else
                                return FakeObject(Connection[Index]) or nil
                            end
                        end
                        
                        function FakeConnectionMeta:__tostring()
                            return "Connection"
                        end
                        
                        FakeConnectionMeta.__metatable = getmetatable(Connection)
                    end
                    
                    function EventMetatable:__index(Index)
                        if InternalTable[Index] then
                            return InternalTable[Index]
                        else
                            return FakeObject(RealObj[Index])
                        end
                    end
                    
                    function EventMetatable:__newindex(Item)
                        return error(("%s cannot be assigned to"):format(Item))
                    end
                    
                    EventMetatable.__metatable = getmetatable(RealEvent)
                    
                    NewFakeObject = NewEvent
                elseif RealType == "table" then
                    NewFakeObject = RealObj
                end
            end
        end

        if NewFakeObject then
            Objects[ObjIndex] = NewFakeObject
            RealObjects[NewFakeObject] = RealObj
            FakeObjects[RealObj] = NewFakeObject 
        end
    end
    
    return unpack(Objects)
end

function FakeInstance(Instance)
    if not IsRealObject(Instance) and not pcall(function() return Instance.ClassName end) then
        return Instance
    end
    
    local Class = Instance.ClassName
    
    local NewInstance = newproxy(true)
    local InstanceMeta = getmetatable(NewInstance)
    
    InstanceMeta.__metatable = getmetatable(Instance)
    
    function InstanceMeta:__tostring()
        return tostring(Instance)
    end
    
    function InstanceMeta:__index(Index)
        local Success, Result = pcall(GetMember, Instance, Index)
        
        if not Success then
            return error(Result,0)
        end
        
        if type(Result) == "function" then
            local Function = GetReplacedFunction("Get", Index)
            local Call, Output

            if not Function then
				Call, Output = true, setfenv(FakeObject(Result), getfenv(2))
            else
                Call, Output = true, setfenv(Function, getfenv(2))
            end

            if not Call then
                return error(Output:match("%S+:%d+: (.*)$") or Output,0)
            end
            
            if type(Output) ~= "function" then
                return FakeObject(Output)
            else
                return Output
            end
        elseif Result then
            return FakeObject(Result)
		elseif not Success then
            return error(Index.." is not a valid member of "..Class)
        end
    end
    
    function InstanceMeta:__newindex(Index, Value)
        local Success, Result
        local Function = GetReplacedFunction("Set", Index)
        
        if Function then
            Success, Result = pcall(SetMemeber, Instance, Index, Function(Instance, Value))
        else
            Success, Result = pcall(SetMemeber, Instance, Index, RealObject(Value))
        end
    end
    
    return NewInstance
end

--[[ External Functions ]]--

--[[ Shared Objects ]]--

local SharedObjects = {
    ["game"] = FakeObject(game),
    ["Game"] = FakeObject(Game),
    ["workspace"] = FakeObject(workspace),
    ["Workspace"] = FakeObject(workspace),
    ["print"] = function(...)
        Gen(RealObject(getfenv(2)("key")), 1, ...)
    end,
    ["warn"] = function(...)
        Gen(RealObject(getfenv(2)("key")), 3, ...)
    end,
	["Instance"] = setmetatable({
        new = function(Class, Parent)
            local Parent = RealObject(Parent)
            
            local Check, Result = pcall(function() return Instance.new(Class, Parent) end)
            
            if not Check then
                return error(Result, 0)
            end
            
            return FakeObject(Result)
        end,
        Lock = Instance.Lock,
        Unlock = Instance.Unlock,
    },{
		__metatable = "This metatable is locked",
	}),
    ["require"] = function(Object)
        return error("temp disabled")
    end,
}

--[[ Creates a new Sandbox ]]--

function Sandbox:CreateSandbox(Table, KillFunction, Owner, Script)
   InternalSandbox[Table] = {}
   local NewSandbox = InternalSandbox[Table]

   Table["Owner"] = FakeObject(Owner)
   Table["owner"] = FakeObject(Owner)
   Table["script"] = FakeObject(Script)
   
   return setmetatable(Table, {
       __index = function(self, index)
          if KillFunction and not KillFunction() then
             return error("Script Ended")
          end
          
          local Original = Environment[index]
          
          if NewSandbox[index] == "nil" then
             return nil
          elseif NewSandbox[index] then
             return NewSandbox[index]
          elseif SharedObjects[index] then
             NewSandbox[index] = SharedObjects[index]
             if type(NewSandbox[index]) == "function" then
                NewSandbox[index] = setfenv(NewSandbox[index], Table)
			 elseif type(NewSandbox[index]) == "table" then
				for i,v in next, NewSandbox[index] do
					if type(v) == "function" and pcall(function() setfenv(v,{}) end) then
						NewSandbox[index][i] = setfenv(v, Table)
					elseif type(v) == "function" and not pcall(function() setfenv(v,{}) end) then
						NewSandbox[index][i] = setfenv(function(...)
							return FakeObject(NewSandbox[index][i](FakeObject(...)))
						end,Table)
					end
				end
             end
             return NewSandbox[index]
          elseif Original then
             NewSandbox[index] = Original
             return NewSandbox[index]
          else
             return _G[index] or nil
          end
       end,
       
       __newindex = function(self, index, value)
          if value == nil then
             NewSandbox[index] = "nil"
          else
             rawset(Table, index, value)
          end
       end,
       
       __metatable = "This metatable is locked",

	   __call = function(self, key)
		  if key == "key" then
		      return Owner
          else
              return error("attempt to call a table value")
          end
	   end,
   })
end

return Sandbox